const mongoose = require('mongoose');

let productSchema = new mongoose.Schema({

	name : {
		type : String,
		required : [true, "Name Required"]
	},
	description : {
		type : String,
		required : [true, "Description Required"]
	},
	price : {
		type : Number,
		required : [true, "Price Required"]
	},
	inventory : {
		type : Number,
		required : [true, "Inventory Required"]
	},
	isAvailable : {
		type : Boolean,
		default : true
	},
	addedOn : {
		type : Date,
		default : new Date()
	},
	customers : [
		{
			userId : {
				type: String,
				required : [true, "User ID is requried"]
			},
			dateOrdered : {
				type : Date,
				default : new Date()
			}

		}
	],
	reviews : [
		{
			userId : {
				type : String,
				required : [true, "User ID is requried"]
			},
			starReview : {
				type : Number,
				required : [true, "Review Required"],
				min : 1,
				max : 5
			},
			comment : {
				type : String,
				minLength : 0,
				maxLength : 200
			}
		}
	]
});

module.exports = mongoose.model("Product", productSchema);