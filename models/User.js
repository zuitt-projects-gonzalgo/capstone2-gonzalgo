const mongoose = require('mongoose');

let userSchema = new mongoose.Schema({

	userName : {
		type : String,
		required : [true, "Username Required"]
	},
	email : {
		type : String,
		required : [true, "Email Required"]
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile No. Required"]
	},
	password : {
		type : String,
		required : [true, "Password Required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders : [
		{
			productId : {
				type : String,
				required : [true, "Product ID Required"]
			},
			productName : {
				type : String,
				required : [true, "Product Name Required"]
			},
			quantity : {
				type : Number,
				required : [true, "Quantity Required"]
			},
			dateOrdered : {
				type : Date,
				default : new Date()
			}
		}
	],
});

module.exports = mongoose.model("User", userSchema); 