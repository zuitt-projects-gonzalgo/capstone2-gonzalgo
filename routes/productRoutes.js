//install express to use  router method
const express = require('express');
const router = express.Router();

//to import product controllers
const productControllers = require('../controllers/productControllers');

//import auth.js
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// R O U T E S

//add products
router.post('/addProducts', verify, verifyAdmin, productControllers.addProduct);

//edit product details
router.put('/:id', verify, verifyAdmin, productControllers.editProductDetails);

//change product availability to false
router.put('/archive/:id', verify, verifyAdmin, productControllers.archive);

//change product availability to true
router.put('/activate/:id', verify, verifyAdmin, productControllers.activate);

//view all available products (customers)
router.get('/', productControllers.viewAllAvailableProducts);

//view all products regardless of availability(admin)
router.get('/allProducts', verify, verifyAdmin, productControllers.getAllProducts);

//view single product
router.get('/:id', productControllers.getProductDetails);

//search a product by name
router.post('/', productControllers.searchProductByName);

//view all orders(admin)
router.get('/allproducts/orders', verify, verifyAdmin, productControllers.viewAllOrders);

//write a review(customers)
router.post('/reviews/:id', verify, productControllers.writeReview);

//export productRoutes
module.exports = router;