//install express to use router method
const express = require('express');
const router = express.Router();

//to import user controllers
const userControllers = require('../controllers/userControllers');

//to import auth.js
const auth = require('../auth');
const {verify, verifyAdmin} = auth;

// R O U T E S

//user registration
router.post('/', userControllers.registerUser);

//login user
router.post('/login', userControllers.loginUser);

//Set Admin functionality
router.put('/setAdmin/:id', verify, verifyAdmin, userControllers.setAdmin);

//add Order
router.post('/orders/addOrder', verify, userControllers.addOrder);

//view myorders(customer)
router.get('/orders', verify, userControllers.viewOrders);

/*//change status of orders(admin)
router.put('/orders/:id', verify, verifyAdmin, userControllers.changeStatus)*/

//export
module.exports = router;