//packages to be installed and load modules
const express = require('express');
const mongoose = require('mongoose');
	//allow backend to connect to frontend
const cors = require('cors');

//make a port and server
//"https://polar-sands-73817.herokuapp.com/ | https://git.heroku.com/polar-sands-73817.git"

const port = process.env.PORT || 4000;
const app = express();

//to import routes
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');

//to connect to database
mongoose.connect("mongodb+srv://admin_gonzalgo:admin169@gonzalgo-169.griu6.mongodb.net/capstone2?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
	//to create notif on connection if successful or not
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB'));

//MIDDLEWARE
app.use(express.json());
app.use(cors());
	//group routes
app.use('/users', userRoutes);
app.use('/products', productRoutes);

//to load server and listen to port
app.listen(port, () => console.log(`Server is running on localhost:${port}`));