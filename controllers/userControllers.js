//import bcrypt module for password security
const bcrypt = require('bcrypt');

//import user model
const User = require('../models/User');

//import product model
const Product = require('../models/Product');

//import auth.js
const auth = require('../auth');


// C O N T R O L L E R S

//user registration
module.exports.registerUser = (req, res) => {
	
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		userName : req.body.userName,
		email : req.body.email,
		mobileNo : req.body.mobileNo,
		password : hashedPW
	});

	User.findOne({email : req.body.email})
	.then(foundEmail => {
		if(foundEmail === null) {
			newUser.save()
			.then(newUser => res.send(newUser))
			.catch(err => res.send(err))
		} else {
			return res.send('Email is already used')
		}
	})
	.catch(err => res.send(err))
};

// L O G I N
module.exports.loginUser = (req, res) => {
	User.findOne({email : req.body.email})
	.then(foundUser => {
		if(foundUser === null) {
			return res.send('User does not exists')
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			if(isPasswordCorrect) {
				return res.send({accessToken : auth.createAccessToken(foundUser)})
			} else {
				return res.send('Password is incorrect')
			}
		}
	})
	.catch(err => res.send(err))
};

//Set Admin
module.exports.setAdmin = (req, res) => {
	let set = {
		isAdmin : true
	}
	User.findByIdAndUpdate(req.params.id, set, {new : true})
	.then(admin => res.send(admin))
	.catch(err => res.send(err))
};

//add Order
module.exports.addOrder = async (req, res) => {
	if(req.user.isAdmin) {
		return res.send('Action Forbidden')
	}

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {
		console.log(user);
		let newOrder = {
			productId : req.body.productId,
			productName : req.body.productName,
			quantity : req.body.quantity,
		}

		user.orders.push(newOrder);
		return user.save().then(user => true).catch(err => err.message);
	})

	if(isUserUpdated !== true) {
		return res.send({message : isUserUpdated})
	}

	let isProductUpdated = await Product.findById(req.body.productId)
	.then(product => {
		console.log(product);
		let ordering = {
			userId : req.user.id
		}

		product.customers.push(ordering);
		return product.save().then(product => true).catch(err => err.message);
	})

	if(isProductUpdated !== true) {
		return res.send({message : isProductUpdated})
	}

	if(isUserUpdated && isProductUpdated) {
		return res.send({message : 'Ordered Succesfully!'})
	}
};

//view myOrders(customers)
module.exports.viewOrders = (req, res) => {
	User.findById(req.user.id)
	.then(result => {
		if(result.orders.length === 0) {
			return res.send('No orders found')
		} else {
			res.send(result.orders)
		}
	})
	.catch(err => res.send(err))
};

/*//change status of orders to shipping(admin)
module.exports.changeStatus = (req, res) =>{
	let changeStatus = {
		status : 'Shipping'
	}
	console.log(req.params.id)
	let order = User.findById(req.params.id)
	.then(shipping => res.send(shipping.orders))
	.catch(err => res.send(err))
};*/