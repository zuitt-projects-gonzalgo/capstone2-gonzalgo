//import product model
const Product = require('../models/Product');

//import auth.js
const auth = require('../auth');


// C O N T R O L L E R S

//add product
module.exports.addProduct = (req, res) => {
	let newProduct = new Product({
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		inventory : req.body.inventory
	});

	Product.findOne({name : req.body.name})
	.then(foundName => {
		if(foundName === null) {
			newProduct.save()
			.then(newproduct => res.send(newproduct))
			.catch(err => res.send(err))
		} else {
			return res.send('Product is already added')
		}
	})
	.catch(err => res.send(err))
};

//edit product details
module.exports.editProductDetails = (req, res) => {
	let editDetails = {
		name : req.body.name,
		description : req.body.description,
		price : req.body.price,
		inventory : req.body.inventory
	};
	Product.findByIdAndUpdate(req.params.id, editDetails, {new : true})
	.select({'isAvailable' : 0, 'addedOn' : 0, 'reviews' : 0, 'customers' : 0})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err))
};

//change product availability to false
module.exports.archive = (req, res) => {
	let deactivate = {
		isAvailable : false
	};
	Product.findByIdAndUpdate(req.params.id, deactivate, {new : true})
	.select({'isAvailable' : 0, 'addedOn' : 0, 'reviews' : 0, 'customers' : 0})
	.then(deactivated => res.send(deactivated))
	.catch(err => res.send(err))
};

//change product availability to true
module.exports.activate = (req, res) => {
	let activate = {
		isAvailable : true
	};
	Product.findByIdAndUpdate(req.params.id, activate, {new : true})
	.select({'isAvailable' : 0, 'addedOn' : 0, 'reviews' : 0, 'customers' : 0})
	.then(activated => res.send(activated))
	.catch(err => res.send(err))
};

//view all available products (customers)
module.exports.viewAllAvailableProducts = (req, res) => {

	Product.find({'isAvailable' : true})
	.select({'isAvailable' : 0, 'addedOn' : 0, 'reviews' : 0, 'customers' : 0})
	.then(viewproducts => res.send(viewproducts))
	.catch(err => res.send(err))
};

//view all products regardless of availability(admin)
module.exports.getAllProducts = (req, res) => {

	Product.find({})
	.then(viewall => res.send(viewall))
	.catch(err => res.send(err))
};

//get Product details
module.exports.getProductDetails = (req, res) => {
	Product.findById(req.params.id)
	.select({'_id' : 0, 'isAvailable' : 0, 'customers' : 0})
	.then(product => res.send(product))
	.catch(err => res.send(err))
};

//search product by name
module.exports.searchProductByName = (req, res) => {
	Product.find({name : {$regex : req.body.name, $options : '$i'}})
	.select({'_id' : 0, 'customers' : 0})
	.then(result => {
		if(result.length === 0) {
			return res.send('No Products Found')
		} else {
			return res.send(result)
		}
	})
	.catch(err => res.send(err))
};

//view all orders(admin)
module.exports.viewAllOrders = (req, res) => {
	Product.find({})
	.select({'_id' : 0, 'customers' : 1, 'name' : 1})
	.then(result => res.send(result))
	.catch(err => res.send(err))
};

//write a review(customers)
module.exports.writeReview = (req, res) => {
	console.log(req.user);

	if(req.user.isAdmin) {
		return res.send('Action Forbidden')
	}
	if(req.user.orders === []) {
		return res.send('No products to review')
	}
	Product.findById(req.params.id)
	.then(product => {
		let newReview = {
			userId : req.user.id,
			starReview : req.body.starReview,
			comment : req.body.comment
		}
		product.reviews.push(newReview)
		return product.save().then(result => res.send(result)).catch(err => res.send(err))
	})
	.catch(err => (err))
};